/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.quakereport;

/**
 * Contain detail information about one earthquake news.
 */
public class EarthquakeModel {

    /**
     * Tag used for debugging purpose
     */
    public static final String LOG_TAG = EarthquakeModel.class.getName();

    /**
     * Magnitude size of the earthquake
     */
    private double mMagnitude;

    /**
     * Name of the city where the earthquake is happened
     */
    private String mLocation;

    /**
     * Date when the earthquake is happened
     */
    private long mTimestamp;

    /**
     * The constructor
     *
     * @param mMagnitude of the earthquake
     * @param mLocation  of the earthquake
     * @param mTimestamp of the earthquake
     */
    public EarthquakeModel(double mMagnitude, String mLocation, long mTimestamp) {
        this.mMagnitude = mMagnitude;
        this.mLocation = mLocation;
        this.mTimestamp = mTimestamp;
    }

    /**
     * Get earthquake magnitude
     *
     * @return earthquake magnitude
     */
    public double getMagnitude() {
        return mMagnitude;
    }

    /**
     * Get earthquake location
     *
     * @return earthquake location
     */
    public String getLocation() {
        return mLocation;
    }

    /**
     * Get date of earthquake
     *
     * @return date of earthquake
     */
    public long getTimestamp() {
        return mTimestamp;
    }

}
