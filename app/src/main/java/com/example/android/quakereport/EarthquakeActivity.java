/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.quakereport;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Main activity class. This function will be called when the app is launched.
 */
public class EarthquakeActivity extends AppCompatActivity {

    /**
     * Tag used for debugging purpose
     */
    public static final String LOG_TAG = EarthquakeActivity.class.getName();

    /**
     * Endpoint of URL that needs to be fetched
     */
    private static final String USGS_URL = "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&eventtype=earthquake&orderby=time&minmag=6&limit=10";

    /**
     * List view to show earthquake details
     */
    private ListView mEarthquakeListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earthquake_activity);

        // Find a reference to the {@link ListView} in the layout
        mEarthquakeListView = (ListView) findViewById(R.id.list);

        // Execute background thread
        new FetchEarthquakeTask().execute(USGS_URL);
    }

    /**
     * Execute GET HTTP request in background thread
     */
    private class FetchEarthquakeTask extends AsyncTask<String, Void, ArrayList<EarthquakeModel>> {

        @Override
        protected ArrayList<EarthquakeModel> doInBackground(String... params) {

            String jsonresponse = "";

            // Check if it's null
            if (params.length < 1 || params[0] == null) {
                return null;
            }

            try {
                URL url = QueryUtils.createUrl(USGS_URL);
                jsonresponse = QueryUtils.makeHttpRequest(url);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return QueryUtils.extractEarthquakes(jsonresponse);
        }

        @Override
        protected void onPostExecute(ArrayList<EarthquakeModel> earthquakeModels) {
            // If there is no result, do nothing.
            if (earthquakeModels == null) {
                return;
            }

            // Create a new {@link ArrayAdapter} of earthquakes
            EarthquakeAdapter adapter = new EarthquakeAdapter(getApplicationContext(), earthquakeModels);

            // Set the adapter on the {@link ListView}
            // so the list can be populated in the user interface
            mEarthquakeListView.setAdapter(adapter);
        }
    }
}
