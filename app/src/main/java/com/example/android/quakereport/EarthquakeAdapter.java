/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.quakereport;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Adapter class before loading Earthquake class to the list view
 */
public class EarthquakeAdapter extends ArrayAdapter<EarthquakeModel> {

    /**
     * Tag used for debugging purpose
     */
    public static final String LOG_TAG = EarthquakeAdapter.class.getName();

    /**
     * The constructor
     *
     * @param context             of application
     * @param earthquakeModelList of earthquake datas
     */
    public EarthquakeAdapter(@NonNull Context context, ArrayList<EarthquakeModel> earthquakeModelList) {
        super(context, 0, earthquakeModelList);
    }

    /**
     * Provides a view for an AdapterView (ListView, GridView, etc.)
     *
     * @param position    The position in the list of data that should be displayed in the
     *                    list item view.
     * @param convertView The recycled view to populate.
     * @param parent      The parent ViewGroup that is used for inflation.
     * @return The View for the position in the AdapterView.
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;

        // Inflate the layout ONLY if it's NULL to be efficient
        // for smooth scrolling
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.earthquake_detail, parent, false);
        }

        // Get earthquake object with the associated position
        EarthquakeModel earthquakeModel = getItem(position);

        // Date object related
        Date dateObject = new Date(earthquakeModel.getTimestamp());
        String formattedDate = formatDate(dateObject);
        String formattedTime = formatTime(dateObject);

        // Show data to magnitude
        TextView magnitude = (TextView) listItemView.findViewById(R.id.magnitude);
        magnitude.setText(formatMagnitude(earthquakeModel.getMagnitude()));

        // Change magnitude circle color
        GradientDrawable magnitudeCircle = (GradientDrawable) magnitude.getBackground();
        magnitudeCircle.setColor(getMagnitudeColor(earthquakeModel.getMagnitude()));

        // Show data to distance
        TextView distance = (TextView) listItemView.findViewById(R.id.location_offset);
        distance.setText(formatDistance(earthquakeModel.getLocation()));

        // Show data to location
        TextView location = (TextView) listItemView.findViewById(R.id.primary_location);
        location.setText(formatCity(earthquakeModel.getLocation()));

        // Show data to date
        TextView date = (TextView) listItemView.findViewById(R.id.date);
        date.setText(formattedDate);

        // Show data to time
        TextView time = (TextView) listItemView.findViewById(R.id.time);
        time.setText(formattedTime);

        return listItemView;
    }

    /**
     * Return the formatted date string (i.e. "Mar 3, 1984") from a Date object.
     *
     * @param dateObject of earthquake
     * @return formatted date string
     */
    private String formatDate(Date dateObject) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");
        return dateFormat.format(dateObject);
    }

    /**
     * Return the formatted date string (i.e. "4:30 PM") from a Date object.
     *
     * @param dateObject of earthquake
     * @return formatted time string
     */
    private String formatTime(Date dateObject) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        return timeFormat.format(dateObject);
    }

    /**
     * Return distance information of location to list view
     *
     * @param location from json data
     * @return formatted distance
     */
    private String formatDistance(String location) {
        String[] info = location.split("of ");
        if (info.length == 2) {
            // information is provided
            return info[0] + "of";
        } else {
            // information is not provided
            return "Near the";
        }
    }

    /**
     * Return city information of location to list view
     *
     * @param location from json data
     * @return formatted city
     */
    private String formatCity(String location) {
        String[] info = location.split("of ");
        if (info.length == 2) {
            // information is provided
            return info[1];
        } else {
            // information is not provided
            return location;
        }
    }

    /**
     * Return one significant format of magnitude
     *
     * @param magnitude from json data
     * @return correctly formatted magnitude value
     */
    private String formatMagnitude(double magnitude) {
        DecimalFormat magnitudeFormat = new DecimalFormat("0.0");
        return magnitudeFormat.format(magnitude);
    }

    /**
     * Get magnitude color based on the magnitude value
     *
     * @param magnitude value defining the color
     * @return color id for every magnitude
     */
    private int getMagnitudeColor(double magnitude) {

        int magnitudeColorResourceId;
        int magnitudeFloor = (int) Math.floor(magnitude);

        switch (magnitudeFloor) {
            case 0:
            case 1:
                magnitudeColorResourceId = R.color.magnitude1;
                break;
            case 2:
                magnitudeColorResourceId = R.color.magnitude2;
                break;
            case 3:
                magnitudeColorResourceId = R.color.magnitude3;
                break;
            case 4:
                magnitudeColorResourceId = R.color.magnitude4;
                break;
            case 5:
                magnitudeColorResourceId = R.color.magnitude5;
                break;
            case 6:
                magnitudeColorResourceId = R.color.magnitude6;
                break;
            case 7:
                magnitudeColorResourceId = R.color.magnitude7;
                break;
            case 8:
                magnitudeColorResourceId = R.color.magnitude8;
                break;
            case 9:
                magnitudeColorResourceId = R.color.magnitude9;
                break;
            default:
                magnitudeColorResourceId = R.color.magnitude10plus;
                break;
        }

        return ContextCompat.getColor(getContext(), magnitudeColorResourceId);
    }

}
